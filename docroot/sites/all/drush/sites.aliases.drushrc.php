<?php

/**
 * @file
 * Aliases for different environments.
 */

$aliases['vdd'] = array(
  'env' => 'vdd',
  'uri' => 'drupal8.dev',
  'root' => '/var/www/vhosts/drupal8.dev/docroot',
  'path-aliases' => array(
    '%drush-script' => 'drush8',
  ),
);

if (!file_exists('/var/www/vhosts/drupal8.dev/docroot')) {
  $aliases['vdd']['remote-host'] = 'dev.local';
  $aliases['vdd']['remote-user'] = 'vagrant';
}
